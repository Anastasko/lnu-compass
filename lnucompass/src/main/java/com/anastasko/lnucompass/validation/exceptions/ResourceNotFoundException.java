package com.anastasko.lnucompass.validation.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class ResourceNotFoundException extends WithHttpStatus {
	
	public ResourceNotFoundException(){
	}
	
	public ResourceNotFoundException(String text){
		super(text);
	}

	public HttpStatus getHttpStatus() {
		return HttpStatus.NOT_FOUND;
	}
}
