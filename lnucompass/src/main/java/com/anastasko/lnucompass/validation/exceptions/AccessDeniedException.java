package com.anastasko.lnucompass.validation.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class AccessDeniedException extends WithHttpStatus {
	
	public AccessDeniedException(){
		
	}
	
	public HttpStatus getHttpStatus() {
		return HttpStatus.FORBIDDEN;
	}
	
}
