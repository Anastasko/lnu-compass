package com.anastasko.lnucompass.validation.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public abstract class WithHttpStatus extends RuntimeException {

	public WithHttpStatus() {
		super();
	}

	public WithHttpStatus(String message) {
		super(message);
	}
	
	public abstract HttpStatus getHttpStatus();
	
}
