package com.anastasko.lnucompass.validation.exceptions;

@SuppressWarnings("serial")
public class DuplicateEmailException extends RuntimeException {

	public DuplicateEmailException(String message) {
		super(message);
	}
}