package com.anastasko.lnucompass.configuration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.anastasko.lnucompass.validation.exceptions.AccessDeniedException;

public class RequestInterceptor extends HandlerInterceptorAdapter {
 
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
    		if (request.getHeader("access-token") != null) {
    			// TODO
    			return true;
    		}
    		if (secured(request.getMethod())) {
    			throw new AccessDeniedException();
    		}
    		return true;
    }

    private boolean secured(String method) {
    		return method.equals("POST") || method.equals("PUT") || method.equals("DELETE");
    }
    
}
