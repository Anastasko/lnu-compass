package com.anastasko.lnucompass.build.codemodel;

import org.springframework.stereotype.Service;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.implementation.AbstractContentEntityServiceImpl;
import com.anastasko.lnucompass.model.enums.EntityTypeName;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;

public class ServiceImplModel extends AbstractEntityClassModel<ServiceImplModel> implements ModelBuilder {

    private JClass systemClass;

    public ServiceImplModel(EntityType entityType, ServiceImplPackageModel serviceImplPackageModel)
            throws JClassAlreadyExistsException, ClassNotFoundException {

        super(entityType, serviceImplPackageModel);

        if (entityType.getIsSystem()) {
            String implName = getCodeModel().getNamingService().systemTypeServiceImplClassName(entityType);
            systemClass = getPackage().owner().ref(implName);
        } else {
            String className = getCodeModel().getNamingService().entityTypeServiceImplName(entityType);
            definedClass = getPackage()._class(className);
            definedClass.annotate(Service.class);
        }
    }

    @Override
    public void build() {

        if (getEntityType().getIsSystem()) {
            return;
        }

        TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());

        definedClass._implements(getCodeModel().getServicePackageModel().getClassModel(getEntityType()).getJClass())
                ._extends(definedClass.owner().ref(AbstractContentEntityServiceImpl.class).narrow(typeModel.getType()));

        createGetEntityClassMethod();
        createGetEntityTypeName();
        createNewInstanceMethod();
    }

    /**
     * used in JPA
     */
    private void createGetEntityClassMethod() {

        TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());

        String methodName = namingService.methodName(MethodKind.GET_ENTITY_CLASS);
        JMethod method = definedClass.method(JMod.PUBLIC,
                definedClass.owner().ref(Class.class).narrow(typeModel.getType()), methodName);
        method.annotate(Override.class);

        method.body()._return(typeModel.getJClass().dotclass());
    }

    /**
     * used in view service
     */
    private void createNewInstanceMethod() {
        TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());
        String methodName = namingService.methodName(MethodKind.NEW_INSTANCE);
        JMethod method = definedClass.method(JMod.PUBLIC, typeModel.getType(), methodName);
        method.annotate(Override.class);
        method.body()._return(JExpr._new(typeModel.getType()));
    }

    /**
     * used in item properties
     */
    private void createGetEntityTypeName() {
        String methodName = namingService.methodName(MethodKind.GET_ENTITY_TYPE_NAME);
        JMethod method = definedClass.method(JMod.PUBLIC,
                definedClass.owner().ref(EntityTypeName.class), methodName);
        method.annotate(Override.class);
        method.body()._return(ref(EntityTypeName.class).staticRef(getEntityType().getTypeName().name()));
    }


    @Override
    protected JType getType() {
        return this.getJClass();
    }

    @Override
    protected JClass getJClass() {
        if (getEntityType().getIsSystem()) {
            return this.systemClass;
        }
        return this.definedClass;
    }
}
