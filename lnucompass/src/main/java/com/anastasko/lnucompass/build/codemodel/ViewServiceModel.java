package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.infrastructure.ViewService;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JType;

public class ViewServiceModel extends AbstractEntityClassModel<ViewServiceModel> implements ModelBuilder {

	private JClass systemClass;

	public ViewServiceModel(EntityType entityType, ViewServicePackageModel viewServicePackageModel) throws JClassAlreadyExistsException, ClassNotFoundException {

		super(entityType, viewServicePackageModel);
		
		if (entityType.getIsSystem()) {
			String interfaceName = getCodeModel().getNamingService().systemTypeViewServiceClassName(entityType);
			systemClass = getPackage().owner().ref(interfaceName);
		} else {
			String className = getCodeModel().getNamingService().entityTypeViewServiceName(entityType);
			definedClass = getPackage()._interface(className);
		}
	}
	
	@Override
	public void build() {

		if (getEntityType().getIsSystem()) {
			return;
		}

		TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());
		ViewModelModel viewModelModel = getCodeModel().getViewModelPackageModel().getClassModel(getEntityType());
		definedClass._extends(definedClass.owner().ref(getBaseServiceClass(getEntityType().getTypeKind()))
				.narrow(typeModel.getType())
				.narrow(viewModelModel.getType()));

	}

	@Override
	protected JType getType() {
		return this.getJClass();
	}
	
	@Override
	protected JClass getJClass() {
		if (getEntityType().getIsSystem()) {
			return this.systemClass;
		}
		return this.definedClass;
	}
	
	private Class<?> getBaseServiceClass(EntityTypeKind entityTypeKind) {

        return ViewService.class;

	}
}
