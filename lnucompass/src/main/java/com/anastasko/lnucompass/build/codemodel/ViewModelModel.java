package com.anastasko.lnucompass.build.codemodel;

import java.util.HashMap;
import java.util.Map;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.build.model.enums.ParameterKind;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.model.view.AbstractEntityViewModel;
import com.anastasko.lnucompass.model.view.UrlResourceViewModel;
import com.sun.codemodel.JAnnotationArrayMember;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class ViewModelModel extends AbstractEntityClassModel<ViewModelModel> implements ModelBuilder {

    public ViewModelModel(EntityType entityType, ViewModelPackageModel viewModelPackageModel) throws JClassAlreadyExistsException, ClassNotFoundException {

        super(entityType, viewModelPackageModel);
        EntityTypeKind typeKind = entityType.getTypeKind();

        if (typeKind == EntityTypeKind.PRIMITIVE) {

            primitiveClass = getPackage().owner()._ref(getCodeModel()
                    .getEntityTypeService()
                    .getPrimitiveEntityViewModelClass(entityType, true));

        } else {
            if (entityType.getIsSystem()) {
                this.systemClass = getPackage().owner().ref(namingService.systemTypeViewModelClassName(entityType));
            } else {
                NamingService namingService = getCodeModel().getNamingService();
                String className = namingService.entityTypeViewModelName(entityType);
                definedClass = getPackage()._class(className);
                definedClass._extends(AbstractEntityViewModel.class);

                getEntityType().getFields().stream()
                        .forEach(f -> addFieldModel(f, new ViewModelFieldModel(f, this)));
            }
        }
    }

    @Override
    public void build() {

        if (getEntityType().getIsSystem() ||
                getEntityType().getTypeKind() == EntityTypeKind.PRIMITIVE) {
            return;
        }
        createNoArgsConstructor();
        createConstructor();
        getEntityType().getFields().stream().filter(f -> f.getFieldKind() == EntityFieldKind.REGULAR)
                .forEach(f -> getFieldModel(f).build());
    }

    private void createNoArgsConstructor() {
        NamingService namingService = getCodeModel().getNamingService();
        JMethod constructor = definedClass.constructor(JMod.PUBLIC);
        for (EntityField field : getEntityType().getFields()) {
            String setterName = namingService.entityFieldSetterName(field);
            if (field.getFieldType().getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE) {
                constructor.body().invoke(setterName).arg(JExpr._new(ref(UrlResourceViewModel.class)));
            }
        }
    }

    private void createConstructor() {
        NamingService namingService = getCodeModel().getNamingService();
        JMethod constructor = definedClass.constructor(JMod.PUBLIC);
        TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());
        JVar entityArg = constructor.param(typeModel.getType(), namingService.parameterName(ParameterKind.ITEM));
        constructor.body().invoke(namingService.methodName(MethodKind.SUPER)).arg(entityArg);
        for (EntityField field : getEntityType().getFields()) {
            String setterName = namingService.entityFieldSetterName(field);
            String getterName = namingService.entityFieldGetterName(field);
            if (field.getFieldKind() == EntityFieldKind.COLLECTION) {

            } else if (field.getFieldKind() == EntityFieldKind.REGULAR && field.getFieldType().getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE) {
                constructor.body().invoke(setterName).arg(JExpr._new(ref(UrlResourceViewModel.class)).arg(entityArg.invoke(getterName)));
            } else if (field.getFieldKind() == EntityFieldKind.REGULAR && field.getFieldType().getTypeKind() == EntityTypeKind.PRIMITIVE) {
                constructor.body().invoke(setterName).arg(entityArg.invoke(getterName));
            } else if (field.getFieldKind() == EntityFieldKind.REGULAR && field.getFieldType().getTypeKind() == EntityTypeKind.ENTITY) {
                constructor.body().invoke(setterName).arg(JExpr._new(ref(AbstractEntityViewModel.class)).arg(entityArg.invoke(getterName)));
            } else {
                throw new RuntimeException("missed field kind ? ");
            }
        }
    }

    @Override
    protected JType getType() {
        if (getEntityType().getTypeKind() == EntityTypeKind.PRIMITIVE) {
            return this.primitiveClass;
        }
        if (getEntityType().getIsSystem()) {
            return this.systemClass;
        }
        return this.definedClass;
    }

    @Override
    protected JClass getJClass() {
        if (getEntityType().getIsSystem()) {
            return this.systemClass;
        }
        return this.definedClass;
    }

    protected void addFieldModel(EntityField entityField, ViewModelFieldModel viewModelFieldModel) {
        fieldModelMap.put(entityField.getId(), viewModelFieldModel);
    }

    public ViewModelFieldModel getFieldModel(EntityField field) {
        return fieldModelMap.get(field.getId());
    }

    public Iterable<ViewModelFieldModel> getFieldModels() {
        return fieldModelMap.values();
    }

    public JAnnotationArrayMember getAnnotationGraphs() {
        return annotationGraphs;
    }

    private JType primitiveClass;
    private JClass systemClass;
    private JAnnotationArrayMember annotationGraphs;
    private Map<Long, ViewModelFieldModel> fieldModelMap = new HashMap<>();

}
