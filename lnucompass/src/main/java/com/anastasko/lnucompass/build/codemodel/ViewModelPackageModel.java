package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JPackage;

public class ViewModelPackageModel extends AbstractEntityPackageModel<ViewModelModel> implements ModelBuilder {

	public ViewModelPackageModel(Iterable<EntityType> entityTypes, JPackage pack, CodeModel codeModel) throws JClassAlreadyExistsException, ClassNotFoundException {
		super(codeModel, pack);
		for(EntityType entityType : entityTypes) {
			addClassModel(entityType, new ViewModelModel(entityType, this));
		}
	}
	
	@Override
	public void build() {
		for (ViewModelModel modelType : getClassModels()) {
			modelType.build();
		}
	}

}
