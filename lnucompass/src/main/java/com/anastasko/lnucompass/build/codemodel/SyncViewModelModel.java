package com.anastasko.lnucompass.build.codemodel;

import java.util.List;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.sun.codemodel.ClassType;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class SyncViewModelModel extends AbstractClassModel<SyncViewModelModel> implements ModelBuilder {

    private JDefinedClass syncCountViewModelClass;

    private JDefinedClass syncActiveViewModelClass;

    private JDefinedClass syncDeletedViewModelClass;

    public SyncViewModelModel(JPackage pack, CodeModel codeModel) throws JClassAlreadyExistsException {
        super(pack, codeModel);
        syncCountViewModelClass = pack._class(JMod.PUBLIC, "SyncCountViewModel", ClassType.CLASS);
        syncActiveViewModelClass = pack._class(JMod.PUBLIC, "SyncActiveViewModel", ClassType.CLASS);
        syncDeletedViewModelClass = pack._class(JMod.PUBLIC, "SyncDeletedViewModel", ClassType.CLASS);
    }

    @Override
    public void build() {
        createSyncCountViewModel();
        createSyncActiveViewModel();
        createSyncDeletedViewModel();
    }

    private void createSyncActiveViewModel() {
        getCodeModel().getEntityTypeService().findAll().stream()
                .filter(t -> t.getTypeKind() == EntityTypeKind.ENTITY)
                .forEach(t -> {
                    JType list = ref(List.class)
                            .narrow(getCodeModel().getViewModelPackageModel().getClassModel(t).getType());
                    JFieldVar f = syncActiveViewModelClass.field(JMod.PRIVATE, list,
                            namingService.toCamelCase(t.getTypeName(), false));

                    JMethod getter = syncActiveViewModelClass.method(JMod.PUBLIC, list, "get" + namingService.entityTypeSimpleName(t));
                    getter.body()._return(f);

                    JMethod setter = syncActiveViewModelClass.method(JMod.PUBLIC, void.class, "set" + namingService.entityTypeSimpleName(t));
                    JVar cntVar = setter.param(list, "items");
                    setter.body().assign(f, cntVar);
                });
    }

    private void createSyncDeletedViewModel() {
        getCodeModel().getEntityTypeService().findAll().stream()
                .filter(t -> t.getTypeKind() == EntityTypeKind.ENTITY)
                .forEach(t -> {
                    JType list = ref(List.class).narrow(Long.class);
                    JFieldVar f = syncDeletedViewModelClass.field(JMod.PRIVATE, list,
                            namingService.toCamelCase(t.getTypeName(), false));

                    JMethod getter = syncDeletedViewModelClass.method(JMod.PUBLIC, list, "get" + namingService.entityTypeSimpleName(t));
                    getter.body()._return(f);

                    JMethod setter = syncDeletedViewModelClass.method(JMod.PUBLIC, void.class, "set" + namingService.entityTypeSimpleName(t));
                    JVar cntVar = setter.param(list, "items");
                    setter.body().assign(f, cntVar);
                });
    }

    private void createSyncCountViewModel() {
        getCodeModel().getEntityTypeService().findAll().stream()
                .filter(t -> t.getTypeKind() == EntityTypeKind.ENTITY)
                .forEach(t -> {
                    JFieldVar f = syncCountViewModelClass.field(JMod.PRIVATE, ref(Long.class),
                            namingService.toCamelCase(t.getTypeName(), false));

                    JMethod getter = syncCountViewModelClass.method(JMod.PUBLIC, ref(Long.class), "get" + namingService.entityTypeSimpleName(t));
                    getter.body()._return(f);

                    JMethod setter = syncCountViewModelClass.method(JMod.PUBLIC, void.class, "set" + namingService.entityTypeSimpleName(t));
                    JVar cntVar = setter.param(ref(Long.class), "cnt");
                    setter.body().assign(f, cntVar);
                });
    }


    public JDefinedClass getSyncCountViewModelClass() {
        return syncCountViewModelClass;
    }

}
