package com.anastasko.lnucompass.build.codemodel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.sun.codemodel.JPackage;

public class AbstractEntityPackageModel<T extends AbstractEntityClassModel<T>> extends AbstractPackageModel<T> {

	protected void addClassModel(EntityType entityType, T classItem) {

		classModelMap.put(entityType.getId(), classItem);
	}
	
	public T getClassModel(EntityType entityType) {
		return classModelMap.get(entityType.getId());
	}

	@Override
	protected Collection<T> getClassModels() {
		return classModelMap.values();
	}

	protected AbstractEntityPackageModel(CodeModel codeModel, JPackage pack) {
		super(codeModel, pack);
	}

	private Map<Long, T> classModelMap = new HashMap<>();
}
