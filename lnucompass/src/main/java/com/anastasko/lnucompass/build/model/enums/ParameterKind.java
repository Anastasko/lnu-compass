package com.anastasko.lnucompass.build.model.enums;

public enum ParameterKind {
    NAME, VALUE, NULLABLE, METHOD, ID, ENTITY, ITEM, ATTRIBUTE_NODES, FETCH, MAPPED_BY, CASCADE, FIND, FIND_MANY, COUNT, ORPHAN_REMOVAL
}
