package com.anastasko.lnucompass.build.codemodel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.ParameterKind;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.build.validation.CodeModelException;
import com.anastasko.lnucompass.model.domain.AbstractContentEntity;
import com.anastasko.lnucompass.model.domain.UrlResource;
import com.sun.codemodel.JAnnotationArrayMember;
import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;

public class TypeModel extends AbstractEntityClassModel<TypeModel> implements ModelBuilder {

    public TypeModel(EntityType entityType, TypePackageModel typePackageModel) throws JClassAlreadyExistsException, ClassNotFoundException {

        super(entityType, typePackageModel);
        EntityTypeKind typeKind = entityType.getTypeKind();

        if (typeKind == EntityTypeKind.PRIMITIVE) {

            primitiveClass = getPackage().owner().
                    _ref(entityType.getPrimitiveEntityType().getPrimitiveEntityTypeClass(true));

        } else {
            if (entityType.getIsSystem()) {
				this.systemClass = ref(entityType.getSystemClass());
            } else {
                String className = namingService.entityTypeName(entityType);
                definedClass = getPackage()._class(className);
                definedClass._extends(getEntityBaseClass(typeKind));

                definedClass.annotate(Entity.class);
                annotationGraphs = definedClass.annotate(NamedEntityGraphs.class)
                        .paramArray(getCodeModel().getNamingService().parameterName(ParameterKind.VALUE));

                /* graph for collections */
                getEntityType().getFields().stream().filter(f -> f.getFieldKind() == EntityFieldKind.COLLECTION).forEach(f -> {
                    JAnnotationUse annotation = annotationGraphs.annotate(NamedEntityGraph.class)
                            .param(namingService.parameterName(ParameterKind.NAME),
                                    namingService.entityFieldGraphName(f));
                    JAnnotationArrayMember nodes = annotation.paramArray(namingService.parameterName(ParameterKind.ATTRIBUTE_NODES));
                    nodes.annotate(NamedAttributeNode.class).param(
                            namingService.parameterName(ParameterKind.VALUE),
                            namingService.entityFieldName(f));
                });

                getEntityType().getFields().stream()
                        .forEach(f -> addFieldModel(f, new FieldModel(f, this)));
            }
        }
    }

    @Override
    public void build() {

        if (getEntityType().getIsSystem() || 
        		getEntityType().getTypeKind() == EntityTypeKind.PRIMITIVE) {
        	return;
        }
        
        createConstructor();
        getEntityType().getFields().stream()
        	.forEach(f -> getFieldModel(f).build());
    }

    private void createConstructor() {
        NamingService namingService = getCodeModel().getNamingService();
        JMethod constructor = definedClass.constructor(JMod.PUBLIC);
        for (EntityField field : getEntityType().getFields()) {
            String setterName = namingService.entityFieldSetterName(field);
            if (field.getFieldKind() == EntityFieldKind.COLLECTION) {
                JType fieldType = getCodeModel().getTypePackageModel().getClassModel(field.getFieldType()).getType();
                constructor.body().invoke(setterName).arg(JExpr._new(definedClass.owner().ref(HashSet.class).narrow(fieldType)));
            }
            if (field.getFieldType().getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE){
                constructor.body().invoke(setterName).arg(JExpr._new(ref(UrlResource.class)));
            }
        }
    }

    @Override
    protected JType getType() {
        if (getEntityType().getTypeKind() == EntityTypeKind.PRIMITIVE) {
            return this.primitiveClass;
        }
        if (getEntityType().getIsSystem()) {
            return this.systemClass;
        }
        return this.definedClass;
    }

    @Override
    protected JClass getJClass() {
        if (getEntityType().getIsSystem()) {
            return this.systemClass;
        }
        return this.definedClass;
    }

    private Class<?> getEntityBaseClass(
            EntityTypeKind typeKind) {

        switch (typeKind) {
            case PRIMITIVE:
                throw new CodeModelException();
            case ENTITY:
                return AbstractContentEntity.class;
            default:
                return null;
        }
    }

    protected void addFieldModel(EntityField entityField, FieldModel fieldModel) {
        fieldModelMap.put(entityField.getId(), fieldModel);
    }

    public FieldModel getFieldModel(EntityField field) {
        return fieldModelMap.get(field.getId());
    }

    public Iterable<FieldModel> getFieldModels() {
        return fieldModelMap.values();
    }

    public JAnnotationArrayMember getAnnotationGraphs() {
        return annotationGraphs;
    }

    private JType primitiveClass;
    private JClass systemClass;
    private JAnnotationArrayMember annotationGraphs;
    private Map<Long, FieldModel> fieldModelMap = new HashMap<>();

}
