package com.anastasko.lnucompass.build;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.anastasko.lnucompass.build.codemodel.CodeModel;
import com.anastasko.lnucompass.build.implementation.EntityTypeServiceImpl;
import com.anastasko.lnucompass.build.implementation.NamingServiceImpl;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityFieldName;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.build.model.view.EntityTypesViewModel;
import com.anastasko.lnucompass.configuration.CoreConfig;
import com.anastasko.lnucompass.model.enums.EntityTypeName;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.writer.FileCodeWriter;

public class Main {

    private static Long ID = 0L;

    public static Long NEXT_ID() {
        return ID++;
    }

    public static List<EntityType> types = new ArrayList<>();

    public static void initTypesAndFields() {

        final EntityType TYPE_NUMBER = EntityType.create(PrimitiveEntityType.INTEGER);
        final EntityType TYPE_STRING = EntityType.create(PrimitiveEntityType.STRING);
        final EntityType TYPE_DOUBLE = EntityType.create(PrimitiveEntityType.DOUBLE);
        final EntityType TYPE_URL_RESOURCE = EntityType.create(PrimitiveEntityType.URL_RESOURCE);

        final EntityType TYPE_MAP = EntityType.create(EntityTypeName.MAP);
        final EntityType TYPE_FACULTY = EntityType.create(EntityTypeName.FACULTY);
        final EntityType TYPE_IOS_ICON = EntityType.create(EntityTypeName.IOS_ICON);
        final EntityType TYPE_MAP_ITEM = EntityType.create(EntityTypeName.MAP_ITEM);
        final EntityType TYPE_ITEM_KIND = EntityType.create(EntityTypeName.ITEM_KIND);
        final EntityType TYPE_CITY_ITEM = EntityType.create(EntityTypeName.CITY_ITEM);
        final EntityType TYPE_ANDROID_ICON = EntityType.create(EntityTypeName.ANDROID_ICON);

        TYPE_FACULTY.withFields(
                EntityField.regularField(TYPE_STRING, EntityFieldName.NAME),
                EntityField.regularField(TYPE_STRING, EntityFieldName.PHONE),
                EntityField.regularField(TYPE_STRING, EntityFieldName.EMAIL),
                EntityField.regularField(TYPE_STRING, EntityFieldName.WEBSITE),
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.ICON).from("/uploads/faculties")
        );

        TYPE_IOS_ICON.withFields(
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.SIZE_2X).from("/uploads/icons/ios"),
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.SIZE_3X).from("/uploads/icons/ios")
        );

        TYPE_ANDROID_ICON.withFields(
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.xxxhdpi),
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.xxhdpi),
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.xhdpi),
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.mdpi),
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.hdpi)
        );

        TYPE_ITEM_KIND.withFields(
                EntityField.regularField(TYPE_STRING, EntityFieldName.NAME),
                EntityField.regularField(TYPE_IOS_ICON, EntityFieldName.IOS_ICON),
                EntityField.regularField(TYPE_IOS_ICON, EntityFieldName.IOS_SELECTED_ICON),
                EntityField.regularField(TYPE_ANDROID_ICON, EntityFieldName.ANDROID_ICON),
                EntityField.regularField(TYPE_ANDROID_ICON, EntityFieldName.ANDROID_SELECTED_ICON)
        );

        TYPE_MAP.withFields(
                EntityField.regularField(TYPE_URL_RESOURCE, EntityFieldName.IMAGE).from("/uploads/maps"),
                EntityField.regularField(TYPE_NUMBER, EntityFieldName.FLOOR),
                EntityField.collectionField(TYPE_MAP_ITEM, EntityFieldName.MAP_ITEMS)
        );

        TYPE_CITY_ITEM.withFields(
                EntityField.regularField(TYPE_STRING, EntityFieldName.NAME),
                EntityField.regularField(TYPE_STRING, EntityFieldName.PLACE_ID),
                EntityField.regularField(TYPE_DOUBLE, EntityFieldName.LONGITUDE),
                EntityField.regularField(TYPE_DOUBLE, EntityFieldName.LATITUDE),
                EntityField.regularField(TYPE_STRING, EntityFieldName.ADDRESS),
                EntityField.collectionField(TYPE_MAP, EntityFieldName.MAPS),
                EntityField.regularField(TYPE_ITEM_KIND, EntityFieldName.KIND),
                EntityField.collectionField(TYPE_FACULTY, EntityFieldName.FACULTIES)
        );

        TYPE_MAP_ITEM.withFields(
                EntityField.regularField(TYPE_STRING, EntityFieldName.NAME),
                EntityField.regularField(TYPE_DOUBLE, EntityFieldName.SQUARE),
                EntityField.regularField(TYPE_STRING, EntityFieldName.ROOM),
                EntityField.regularField(TYPE_ITEM_KIND, EntityFieldName.KIND),
                EntityField.regularField(TYPE_FACULTY, EntityFieldName.FACULTY)
        );

    }

    public static void main(String[] args) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        System.out.println("Start");
        initTypesAndFields();
        String path = Paths.get("").toAbsolutePath() + File.separator + "src" + File.separator + "main" + File.separator + "java";
        CodeModel codeModel = new CodeModel(new NamingServiceImpl(), new EntityTypeServiceImpl());
        codeModel.build();
        codeModel.getCodeModel().build(new FileCodeWriter(new File(path)));
        System.out.println("Build OK");

        ObjectMapper objectMapper = (new CoreConfig()).objectMapper();
        String data = objectMapper.writeValueAsString(new EntityTypesViewModel(types));
        String resources = Paths.get("").toAbsolutePath() +
                File.separator + "src" +
                File.separator + "main" +
                File.separator + "resources" +
                File.separator + "static" +
                File.separator + "domain.json";
        FileUtils.writeStringToFile(new File(resources), data);
    }

}
