package com.anastasko.lnucompass.build.model.domain;

import com.anastasko.lnucompass.build.Main;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.EntityFieldName;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.model.domain.AbstractEntity;
import com.anastasko.lnucompass.validation.exceptions.ServiceException;

public class EntityField extends AbstractEntity {

    private EntityFieldKind fieldKind;

    private EntityType fieldType;

    private EntityType owner;

    private EntityFieldName fieldName;

    private Long relativeOrder;

    private String prefixPath;

    private EntityField() {
        this.setId(Main.NEXT_ID());
        setPrefixPath("");
    }

    public static EntityField regularField(EntityType fieldType, EntityFieldName fieldName) {
        EntityField field = new EntityField();
        field.setFieldKind(EntityFieldKind.REGULAR);
        field.setFieldType(fieldType);
        field.setFieldName(fieldName);
        return field;
    }

    public static EntityField collectionField(EntityType fieldType, EntityFieldName fieldName) {
        EntityField field = new EntityField();
        field.setFieldKind(EntityFieldKind.COLLECTION);
        field.setFieldType(fieldType);
        field.setFieldName(fieldName);
        return field;
    }

    public EntityField from(String prefix){
        if (this.fieldType.getPrimitiveEntityType() != PrimitiveEntityType.URL_RESOURCE){
            throw new ServiceException("from(...) applicable only to URL_RESOURCE");
        }
        setPrefixPath(prefix);
        return this;
    }

    public Long getRelativeOrder() {
        return relativeOrder;
    }

    public void setRelativeOrder(Long relativeOrder) {
        this.relativeOrder = relativeOrder;
    }

    public boolean isIncludedInSync() {
		return getFieldKind() == EntityFieldKind.COLLECTION ||
                getFieldType().getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE;
	}

    public EntityType getOwner() {
        return owner;
    }

    public void setOwner(EntityType owner) {
        this.owner = owner;
    }

    public EntityFieldKind getFieldKind() {
        return fieldKind;
    }

    public void setFieldKind(EntityFieldKind fieldKind) {
        this.fieldKind = fieldKind;
    }

    public EntityType getFieldType() {
        return fieldType;
    }

    public void setFieldType(EntityType fieldType) {
        this.fieldType = fieldType;
    }

    public EntityFieldName getFieldName() {
        return fieldName;
    }

    public void setFieldName(EntityFieldName fieldName) {
        this.fieldName = fieldName;
    }

    public String getPrefixPath() {
        return prefixPath;
    }

    public void setPrefixPath(String prefixPath) {
        this.prefixPath = prefixPath;
    }

}
