package com.anastasko.lnucompass.build.infrastructure;

import java.util.List;

import com.anastasko.lnucompass.build.model.domain.EntityType;

public interface EntityTypeService {

    Class<?> getPrimitiveEntityViewModelClass(EntityType entityType, boolean isNullable);

    List<EntityType> findAll();

}