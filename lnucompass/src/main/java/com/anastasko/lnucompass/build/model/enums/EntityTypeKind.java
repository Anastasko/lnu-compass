package com.anastasko.lnucompass.build.model.enums;

public enum EntityTypeKind {
	
	PRIMITIVE,
	ENTITY

}
