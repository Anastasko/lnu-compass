package com.anastasko.lnucompass.build.codemodel;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.build.model.enums.VariableKind;
import com.anastasko.lnucompass.implementation.AbstractViewServiceImpl;
import com.anastasko.lnucompass.infrastructure.ContentEntityService;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JOp;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class ViewServiceImplModel extends AbstractEntityClassModel<ViewServiceImplModel> implements ModelBuilder {

    private final JType itemViewModelType;
    private final JType itemType;
    private JClass systemClass;

    public ViewServiceImplModel(EntityType entityType, ViewServiceImplPackageModel viewServiceImplPackageModel)
            throws JClassAlreadyExistsException, ClassNotFoundException {

        super(entityType, viewServiceImplPackageModel);

        if (entityType.getIsSystem()) {
            String implName = getCodeModel().getNamingService().systemTypeServiceImplClassName(entityType);
            systemClass = getPackage().owner().ref(implName);
        } else {
            String className = getCodeModel().getNamingService().entityTypeViewServiceImplName(entityType);
            definedClass = getPackage()._class(className);
            definedClass.annotate(Service.class);
        }

        ViewModelModel viewModelModel = getCodeModel().getViewModelPackageModel().getClassModel(getEntityType());
        this.itemViewModelType = viewModelModel.getType();

        TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());
        this.itemType = typeModel.getType();
    }

    @Override
    public void build() {

        if (getEntityType().getIsSystem()) {
            return;
        }

        definedClass._extends(ref(AbstractViewServiceImpl.class)
                .narrow(itemType)
                .narrow(itemViewModelType)).
                _implements(getCodeModel().getViewServicePackageModel()
                        .getClassModel(getEntityType()).getJClass());

        createToViewMethod();
        createMergeFieldsMethod();
        createGetEntityService();
    }

    /**
     * used in collector
     */
    private void createToViewMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, itemViewModelType,
                namingService.methodName(MethodKind.TO_VIEW));
        method.annotate(Override.class);
        method.annotate(Transactional.class);
        JVar entity = method.param(itemType, namingService.variableName(VariableKind.ENTITY));
        method.body()._return(JExpr._new(itemViewModelType).arg(entity));
    }

    /**
     * common for create & update
     */
    private void createMergeFieldsMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, void.class,
                namingService.methodName(MethodKind.MERGE_FIELDS));
        method.annotate(Override.class);
        method.annotate(Transactional.class);
        JVar entity = method.param(itemType, namingService.variableName(VariableKind.ENTITY));
        JVar item = method.param(itemViewModelType, namingService.variableName(VariableKind.ITEM));

        for (EntityField field : getEntityType().getFields()) {
            if (field.getFieldKind() == EntityFieldKind.REGULAR && field.getFieldType().getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE) {
                method.body().add(JExpr.direct("urlResourceViewService").invoke(namingService.methodName(MethodKind.MERGE_FIELDS))
                        .arg(entity.invoke(namingService.entityFieldGetterName(field)))
                        .arg(item.invoke(namingService.entityFieldGetterName(field))));
            } else if (field.getFieldKind() == EntityFieldKind.REGULAR && field.getFieldType().getTypeKind() == EntityTypeKind.PRIMITIVE) {
                method.body().add(entity.invoke(namingService.entityFieldSetterName(field))
                        .arg(item.invoke(namingService.entityFieldGetterName(field))));
            } else if (field.getFieldKind() == EntityFieldKind.REGULAR && field.getFieldType().getTypeKind() == EntityTypeKind.ENTITY) {
                JInvocation id = item.invoke(namingService.entityFieldGetterName(field))
                        .invoke(namingService.methodName(MethodKind.GET_ID));
                JExpression reference = JOp.cond(item.invoke(namingService.entityFieldGetterName(field)).eq(JExpr._null()),
                        JExpr._null(),
                        getAutowiredServiceField(field.getFieldType())
                        .invoke(namingService.methodName(MethodKind.GET_REFERENCE)).arg(id));
                method.body().add(entity.invoke(namingService.entityFieldSetterName(field))
                        .arg(reference));
            }
        }
    }

    /**
     * used in abstract view service
     */
    private void createGetEntityService() {
        JMethod method = definedClass.method(JMod.PUBLIC, ref(ContentEntityService.class).narrow(itemType),
                namingService.methodName(MethodKind.GET_ENTITY_SERVICE));
        method.annotate(Override.class);
        method.annotate(Transactional.class);
        method.body()._return(getAutowiredServiceField(getEntityType()));
    }

    @Override
    protected JType getType() {
        return this.getJClass();
    }

    @Override
    protected JClass getJClass() {
        if (getEntityType().getIsSystem()) {
            return this.systemClass;
        }
        return this.definedClass;
    }
}
