package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.model.domain.EntityType;

public class AbstractEntityClassModel<T extends AbstractEntityClassModel<T>> extends AbstractClassModel<T> {
	
	public AbstractEntityClassModel(EntityType entityType, AbstractEntityPackageModel<T> owner) {
		
		super(owner);
		this.entityType = entityType;
	}

	protected EntityType getEntityType() {
		return entityType;
	}
	
	private EntityType entityType;
}
