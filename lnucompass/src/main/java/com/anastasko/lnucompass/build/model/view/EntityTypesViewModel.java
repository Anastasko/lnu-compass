package com.anastasko.lnucompass.build.model.view;

import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.model.view.EntitiesViewModel;

public class EntityTypesViewModel extends EntitiesViewModel<EntityTypeViewModel> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1695887550257763157L;

	public EntityTypesViewModel(Iterable<EntityType> types) {
        for(EntityType type : types){
            add(new EntityTypeViewModel(type));
        }
    }
}
