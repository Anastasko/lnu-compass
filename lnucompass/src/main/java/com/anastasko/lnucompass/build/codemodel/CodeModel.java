package com.anastasko.lnucompass.build.codemodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anastasko.lnucompass.build.infrastructure.EntityTypeService;
import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JPackage;

public class CodeModel implements ModelBuilder {

    public CodeModel(
            NamingService namingService,
            EntityTypeService entityTypeService
    )
            throws JClassAlreadyExistsException, ClassNotFoundException {

        this.namingService = namingService;
        this.entityTypeService = entityTypeService;

        codeModel = new JCodeModel();
        JPackage servicePackage = codeModel._package(getPackageName("service.api.package"));
        JPackage serviceImplPackage = codeModel._package(getPackageName("service.impl.api.package"));
        JPackage entityPackage = codeModel._package(getPackageName("model.domain.api.package"));
        JPackage viewPackage = codeModel._package(getPackageName("model.view.api.package"));
        JPackage controllerPackage = codeModel._package(getPackageName("controller.api.package"));
        JPackage viewServicePackage = codeModel._package(getPackageName("view.service.api.package"));
        JPackage viewServiceImplPackage = codeModel._package(getPackageName("view.service.impl.api.package"));
        JPackage syncServiceImplPackage = codeModel._package(getPackageName("sync.service.impl.api.package"));
        JPackage syncViewModelPackage = codeModel._package(getPackageName("sync.view.model.api.package"));

        List<EntityType> types = entityTypeService.findAll();

        typePackageModel = new TypePackageModel(types, entityPackage, this);
        servicePackageModel = new ServicePackageModel(types, servicePackage, this);
        serviceImplPackageModel = new ServiceImplPackageModel(types, serviceImplPackage, this);
        viewModelPackageModel = new ViewModelPackageModel(types, viewPackage, this);
        controllerPackageModel = new ControllerPackageModel(types, controllerPackage, this);
        viewServicePackageModel = new ViewServicePackageModel(types, viewServicePackage, this);
        viewServiceImplPackageModel = new ViewServiceImplPackageModel(types, viewServiceImplPackage, this);
        syncServiceImplPackageModel = new SyncServiceImplModel(syncServiceImplPackage, this);
        syncViewModelModel = new SyncViewModelModel(syncViewModelPackage, this);
    }

    @Override
    public void build() {
        typePackageModel.build();
        servicePackageModel.build();
        serviceImplPackageModel.build();
        viewModelPackageModel.build();
        controllerPackageModel.build();
        viewServicePackageModel.build();
        viewServiceImplPackageModel.build();
        syncServiceImplPackageModel.build();
        syncViewModelModel.build();
    }

    private String getPackageName(String property) {
        Map<String, String> map = new HashMap<>();
        map.put("view.service.api.package", "com.anastasko.lnucompass.api.infrastructure.view");
        map.put("view.service.impl.api.package", "com.anastasko.lnucompass.api.component.view");
        map.put("service.api.package", "com.anastasko.lnucompass.api.infrastructure.service");
        map.put("service.impl.api.package", "com.anastasko.lnucompass.api.component.service");
        map.put("model.domain.api.package", "com.anastasko.lnucompass.api.model.domain");
        map.put("model.view.api.package", "com.anastasko.lnucompass.api.model.view");
        map.put("controller.api.package", "com.anastasko.lnucompass.api.controller");
        map.put("sync.service.impl.api.package", "com.anastasko.lnucompass.api.component.sync");
        map.put("sync.view.model.api.package", "com.anastasko.lnucompass.api.model.view.sync");
        return map.get(property);
    }

    private JCodeModel codeModel;
    private NamingService namingService;
    private EntityTypeService entityTypeService;
    private TypePackageModel typePackageModel;
    private ServicePackageModel servicePackageModel;
    private ServiceImplPackageModel serviceImplPackageModel;
    private ViewModelPackageModel viewModelPackageModel;
    private ControllerPackageModel controllerPackageModel;
    private ViewServicePackageModel viewServicePackageModel;
    private ViewServiceImplPackageModel viewServiceImplPackageModel;
    private SyncServiceImplModel syncServiceImplPackageModel;
    private SyncViewModelModel syncViewModelModel;

    public JCodeModel getCodeModel() {
        return codeModel;
    }

    public ControllerPackageModel getControllerPackageModel() {
        return controllerPackageModel;
    }

    public NamingService getNamingService() {
        return namingService;
    }

    public EntityTypeService getEntityTypeService() {
        return entityTypeService;
    }

    public TypePackageModel getTypePackageModel() {
        return typePackageModel;
    }

    public ServicePackageModel getServicePackageModel() {
        return servicePackageModel;
    }

    public ServiceImplPackageModel getServiceImplPackageModel() {
        return serviceImplPackageModel;
    }

    public ViewModelPackageModel getViewModelPackageModel() {
        return viewModelPackageModel;
    }

    public ViewServicePackageModel getViewServicePackageModel() {
        return viewServicePackageModel;
    }

    public SyncViewModelModel getSyncViewModelModel() {
        return syncViewModelModel;
    }
}
