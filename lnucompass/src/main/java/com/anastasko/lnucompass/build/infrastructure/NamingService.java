package com.anastasko.lnucompass.build.infrastructure;

import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.FieldKind;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.build.model.enums.ParameterKind;
import com.anastasko.lnucompass.build.model.enums.VariableKind;

public interface NamingService {

	String toCamelCase(Enum<?> item, boolean startWithUpperCase);

	String methodName(MethodKind methodKind);

	String entityTypeName(EntityType entityType);

	String entityFieldName(EntityField field);

	String entityFieldVarName(EntityField field);

	String entityFieldGetterName(EntityField field);

	String entityFieldSetterName(EntityField field);

	String entityTypeServiceName(EntityType entityType);

	String entityTypeServiceImplName(EntityType entityType);

	String systemTypeServiceClassName(EntityType entityType) throws ClassNotFoundException;

	String systemTypeServiceImplClassName(EntityType entityType);

	String entityTypeServiceFieldName(EntityType entityType);

	String entityTypeVarName(EntityType entityType);

	String entityTypeVarName();

	String entityTypeControllerName(EntityType entityType);

	String entityTypeControllerRequestMappingValue(EntityType entityType);

	String emptyString();

	String joinTableName(EntityField entityField);

	String entityGetterName(String variableName);

	String entitySetterName(String variableName);

	String parameterName(ParameterKind parameterKind);

	String variableName(VariableKind variableKind);

	String fieldName(FieldKind fieldKind);

	String entityTypeViewModelName(EntityType entityType);

    String resourceNotFoundExceptionMessage(EntityType entityType);

    String entityTypeViewServiceName(EntityType entityType);

    String entityTypeViewServiceImplName(EntityType entityType);

    String entityTypeViewServiceFieldName(EntityType entityType);

    String entityFieldGraphName(EntityField f);

	String systemTypeViewServiceClassName(EntityType entityType);

	String systemTypeViewModelClassName(EntityType entityType);

    String entityTypeSimpleSpacedName(EntityType entityType);

	String entityTypeFieldSimpleSpacedName(EntityField entityField);

	String entityTypeSimpleName(EntityType entityType, boolean upperCaseFirstLetter);

	String entityTypeSimpleName(EntityType t);
}
