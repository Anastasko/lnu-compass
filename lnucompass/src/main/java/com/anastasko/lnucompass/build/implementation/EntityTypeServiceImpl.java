package com.anastasko.lnucompass.build.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.anastasko.lnucompass.build.Main;
import com.anastasko.lnucompass.build.infrastructure.EntityTypeService;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.model.view.UrlResourceViewModel;

@Service
public class EntityTypeServiceImpl implements EntityTypeService {

    @Override
    public Class<?> getPrimitiveEntityViewModelClass(EntityType entityType, boolean isNullable) {
        if (entityType.getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE){
            return UrlResourceViewModel.class;
        }
        return entityType.getPrimitiveEntityType().getPrimitiveEntityTypeClass(isNullable);
    }

    @Override
    public List<EntityType> findAll() {
        return Main.types;
    }

}
