package com.anastasko.lnucompass.build.infrastructure;

public interface ModelBuilder {

	void build();
}
