package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.model.view.AbstractEntityViewModel;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class ViewModelFieldModel implements ModelBuilder {

    public static final int DECIMAL_PRECISION = 19;

    private EntityField entityField;

    private ViewModelModel owner;

    private JFieldVar fieldVar;

    private JMethod getterMethod;

    private JMethod setterMethod;

    public ViewModelFieldModel(EntityField entityField, ViewModelModel viewModelModel) {

        this.entityField = entityField;
        this.owner = viewModelModel;
    }

    @Override
    public void build() {
        createField();
        createGetter();
        createSetter();
    }

    private void createField() {

        NamingService namingService = getOwner().getCodeModel().getNamingService();
        String fieldName = namingService.entityFieldName(entityField);

        EntityType fieldType = entityField.getFieldType();

        ViewModelPackageModel parent = (ViewModelPackageModel) owner.getOwner();
        ViewModelModel fieldTypeModel = parent.getClassModel(fieldType);
        
        JType fieldClass = getFieldClass(fieldTypeModel);
        fieldVar = owner.getDefinedClass().field(JMod.PRIVATE, fieldClass, fieldName);
    }

    private JType getFieldClass(ViewModelModel fieldViewModelModel) {

        switch (entityField.getFieldKind()) {
            case REGULAR:
                return fieldViewModelModel.getEntityType().getTypeKind() == EntityTypeKind.PRIMITIVE
                 		? fieldViewModelModel.getType()
                		: owner.getPackage().owner().ref(AbstractEntityViewModel.class);
		default:
			break;
        }
        throw new RuntimeException("missing field kind ? ");
    }

    private void createGetter() {

        NamingService namingService = owner.getCodeModel().getNamingService();
        String getterName = namingService.entityFieldGetterName(entityField);

        getterMethod = owner.getDefinedClass().method(JMod.PUBLIC, fieldVar.type(), getterName);
        getterMethod.body()._return(fieldVar);
    }

    private void createSetter() {

        NamingService namingService = owner.getCodeModel().getNamingService();
        String setterName = namingService.entityFieldSetterName(entityField);
        setterMethod = owner.getDefinedClass().method(JMod.PUBLIC, void.class, setterName);
        JVar value = setterMethod.param(fieldVar.type(), fieldVar.name());
        setterMethod.body().assign(JExpr._this().ref(fieldVar), value);
    }

    public ViewModelModel getOwner() {
        return owner;
    }

    public EntityField getEntityField() {
        return entityField;
    }
}
