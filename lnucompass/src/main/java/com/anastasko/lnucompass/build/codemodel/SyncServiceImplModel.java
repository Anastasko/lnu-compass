package com.anastasko.lnucompass.build.codemodel;

import org.springframework.stereotype.Service;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.build.model.enums.VariableKind;
import com.anastasko.lnucompass.implementation.AbstractSyncService;
import com.anastasko.lnucompass.infrastructure.SyncService;
import com.anastasko.lnucompass.model.view.SyncModels;
import com.anastasko.lnucompass.model.view.SyncViewModel;
import com.sun.codemodel.ClassType;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class SyncServiceImplModel extends AbstractClassModel<SyncServiceImplModel> implements ModelBuilder {

    public SyncServiceImplModel(JPackage syncServiceImplPackage, CodeModel codeModel) throws JClassAlreadyExistsException {
        super(syncServiceImplPackage, codeModel);
        definedClass = getPackage()._class(JMod.PUBLIC, "SyncServiceImpl", ClassType.CLASS);
        definedClass._extends(AbstractSyncService.class);
        definedClass._implements(SyncService.class);
        definedClass.annotate(Service.class);
    }

    @Override
    public void build() {
        createCountMethod();
        createSyncMethod();
    }

    private void createSyncMethod() {
        JType returnType = ref(SyncViewModel.class);
        JMethod method = definedClass.method(JMod.PUBLIC, returnType,
                namingService.methodName(MethodKind.SYNC));
        method.annotate(Override.class);
        JVar syncModels = method.param(ref(SyncModels.class), "models");
        JVar res = method.body().decl(returnType, namingService.variableName(VariableKind.RESULT),
                JExpr._new(returnType));
        getCodeModel().getEntityTypeService().findAll().stream()
                .filter(t -> t.getTypeKind() == EntityTypeKind.ENTITY)
                .forEach(t -> {
                    String typeName = namingService.entityTypeSimpleName(t);
                    method.body().invoke("sync")
                            .arg(syncModels)
                            .arg(getAutowiredServiceField(t))
                            .arg(getAutowiredViewServiceField(t))
                            .arg(res.ref("getActive()::set" + typeName))
                            .arg(res.ref("getDeleted()::set" + typeName));
                });
        method.body()._return(res);
    }

    private void createCountMethod() {
        JType returnType = getCodeModel().getSyncViewModelModel().getSyncCountViewModelClass();
        JMethod method = definedClass.method(JMod.PUBLIC, returnType,
                namingService.methodName(MethodKind.COUNT));
        method.annotate(Override.class);
        JVar res = method.body().decl(returnType, namingService.variableName(VariableKind.RESULT),
                JExpr._new(returnType));
        getCodeModel().getEntityTypeService().findAll().stream()
                .filter(t -> t.getTypeKind() == EntityTypeKind.ENTITY)
                .forEach(t -> {
                    method.body().add(res.invoke("set" + namingService.entityTypeSimpleName(t))
                            .arg(JExpr.lit(0L).plus(getAutowiredViewServiceField(t)
                                    .invoke(namingService.methodName(MethodKind.FIND_ALL))
                                    .invoke(namingService.methodName(MethodKind.SIZE)))));
                });
        method.body()._return(res);
    }

}
