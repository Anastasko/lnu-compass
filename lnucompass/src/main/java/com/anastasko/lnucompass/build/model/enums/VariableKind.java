package com.anastasko.lnucompass.build.model.enums;

public enum VariableKind {
    ID, ITEMS, ENTITY, VIEW, AFTER_DATE, ARGS, IDS, MAP, RESULT, OBJECT_NODE, MODIFIED, DATA_NODE, FIELDS, RESPONSE, ITEM, VERSION
}
