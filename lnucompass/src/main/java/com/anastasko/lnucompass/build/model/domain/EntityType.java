package com.anastasko.lnucompass.build.model.domain;

import java.util.ArrayList;
import java.util.List;

import com.anastasko.lnucompass.build.Main;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.EntityFieldName;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.model.domain.AbstractEntity;
import com.anastasko.lnucompass.model.enums.EntityTypeName;

public class EntityType extends AbstractEntity {

    private EntityTypeName typeName;

    private EntityTypeKind typeKind;

    private PrimitiveEntityType primitiveEntityType;

    private List<EntityField> fields;
    
    private Class<?> systemClass;

    private EntityType(){
        this.setId(Main.NEXT_ID());
        fields = new ArrayList<>();
        Main.types.add(this);
    }

    public static EntityType create(PrimitiveEntityType primitiveEntityType){
        EntityType type = new EntityType();
        type.setTypeKind(EntityTypeKind.PRIMITIVE);
        type.setPrimitiveEntityType(primitiveEntityType);
        type.setTypeName(EntityTypeName.UNDEFINED);
        return type;
    }

    public static EntityType create(EntityTypeName typeName){
        EntityType type = new EntityType();
        type.setTypeKind(EntityTypeKind.ENTITY);
        type.setTypeName(typeName);
        return type;
    }

	public static EntityType fromClass(Class<?> systemClass) {
		EntityType type = new EntityType();
		type.setSystemClass(systemClass);
		type.setTypeKind(EntityTypeKind.ENTITY);
		return type;
	}

	public EntityType withFields(EntityField ... fields){
        for(EntityField field : fields) {
            addField(field);
        }
        return this;
    }

    private void addField(EntityField field) {
    	getFields().add(field);
        field.setOwner(this);
        field.setRelativeOrder(new Long(this.getFields().size()));
    	if (field.getFieldKind() == EntityFieldKind.COLLECTION){
    	    boolean exists = false;
    	    for(EntityField f : field.getFieldType().getFields()){
    	        if (f.getFieldName() == EntityFieldName.OWNER){
    	            exists = true;
    	            break;
                }
            }
    	    if (!exists) {
                field.getFieldType().addField(EntityField.regularField(this, EntityFieldName.OWNER));
            }
    	}
	}

	public EntityTypeName getTypeName() {
        return typeName;
    }

    public void setTypeName(EntityTypeName typeName) {
        this.typeName = typeName;
    }

    public List<EntityField> getFields() {
        return fields;
    }

    public void setFields(List<EntityField> fields) {
        this.fields = fields;
    }

    public EntityTypeKind getTypeKind() {
        return typeKind;
    }

    public void setTypeKind(EntityTypeKind typeKind) {
        this.typeKind = typeKind;
    }

    public PrimitiveEntityType getPrimitiveEntityType() {
        return primitiveEntityType;
    }

    public void setPrimitiveEntityType(PrimitiveEntityType primitiveEntityType) {
        this.primitiveEntityType = primitiveEntityType;
    }

    public Class<?> getSystemClass() {
		return systemClass;
	}

	public void setSystemClass(Class<?> systemClass) {
		this.systemClass = systemClass;
	}

	public boolean getIsSystem() {
		return systemClass != null;
	}

}