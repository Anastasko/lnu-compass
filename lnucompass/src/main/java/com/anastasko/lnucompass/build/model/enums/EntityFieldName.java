package com.anastasko.lnucompass.build.model.enums;

public enum EntityFieldName {

    MAPS, LONGITUDE, LATITUDE, NAME, KIND, FLOOR, OWNER,
    CITY_ITEMS, SIZE_3X, SIZE_2X,
    xxxhdpi, xxhdpi, xhdpi, mdpi, hdpi,
    ANDROID_ICON, IOS_ICON, Y, X, MAP_ITEMS,
    ADDRESS, IOS_SELECTED_ICON, ANDROID_SELECTED_ICON,
    ICON, EMAIL, PHONE, FACULTIES,
    WEBSITE, FACULTY, SQUARE, ROOM, PLACE_ID, TRANSACTIONS_COMPLETED, IMAGE


}
