package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JPackage;

public class ControllerPackageModel extends AbstractEntityPackageModel<ControllerModel> implements ModelBuilder {

    protected ControllerPackageModel(Iterable<EntityType> entityTypes, JPackage pack, CodeModel codeModel) throws JClassAlreadyExistsException {

        super(codeModel, pack);

        for (EntityType entityType : entityTypes) {
            if (entityType.getTypeKind() == EntityTypeKind.ENTITY){
                addClassModel(entityType, new ControllerModel(entityType, this));
            }
        }
    }

    @Override
    public void build() {
        for (ControllerModel controllerModel : getClassModels()) {
            controllerModel.build();
        }
    }

}
