package com.anastasko.lnucompass.build.model.view;

import com.anastasko.lnucompass.build.implementation.NamingServiceImpl;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.model.view.AbstractEntityViewModel;

public class EntityFieldViewModel extends AbstractEntityViewModel {

    private EntityFieldKind kind;

    private AbstractEntityViewModel type;

    private String name;

    private String label;

    private Long order;

    private String prefixPath;

    public EntityFieldViewModel(EntityField field) {
        super(field);
        NamingService mn = new NamingServiceImpl();
        setName(mn.entityFieldName(field));
        setKind(field.getFieldKind());
        setType(new AbstractEntityViewModel(field.getFieldType()));
        setLabel(mn.entityTypeFieldSimpleSpacedName(field));
        setOrder(field.getRelativeOrder());
        setPrefixPath(field.getPrefixPath());
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public EntityFieldKind getKind() {
        return kind;
    }

    public void setKind(EntityFieldKind fieldKind) {
        this.kind = fieldKind;
    }

    public AbstractEntityViewModel getType() {
        return type;
    }

    public void setType(AbstractEntityViewModel fieldType) {
        this.type = fieldType;
    }

    public String getName() {
        return name;
    }

    public void setName(String fieldName) {
        this.name = fieldName;
    }

    public String getPrefixPath() {
        return prefixPath;
    }

    public void setPrefixPath(String prefixPath) {
        this.prefixPath = prefixPath;
    }
}
