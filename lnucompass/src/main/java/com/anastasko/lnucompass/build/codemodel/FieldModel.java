package com.anastasko.lnucompass.build.codemodel;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.ParameterKind;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.build.validation.CodeModelException;
import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class FieldModel implements ModelBuilder {

    public static final int DECIMAL_PRECISION = 19;

    private EntityField entityField;

    private TypeModel owner;

    private JFieldVar fieldVar;

    private JMethod getterMethod;

    private JMethod setterMethod;

    public FieldModel(EntityField entityField, TypeModel typeModel) {

        this.entityField = entityField;
        this.owner = typeModel;
    }

    @Override
    public void build() {
        createField();
        createGetter();
        createSetter();
    }

    private void createField() {

        NamingService namingService = getOwner().getCodeModel().getNamingService();
        String fieldName = namingService.entityFieldName(entityField);

        EntityType fieldType = entityField.getFieldType();

        TypePackageModel parent = (TypePackageModel) owner.getOwner();
        TypeModel fieldTypeModel = parent.getClassModel(fieldType);
        EntityTypeKind fieldTypeKind = fieldTypeModel.getEntityType().getTypeKind();
        JType fieldClass = getFieldClass(fieldTypeModel);
        fieldVar = owner.getDefinedClass().field(JMod.PRIVATE, fieldClass, fieldName);


        JAnnotationUse annotation = fieldVar.annotate(getFieldAnnotationClass(fieldType));

        if (entityField.getFieldKind() == EntityFieldKind.COLLECTION) {
            annotation.param(namingService.parameterName(ParameterKind.FETCH), FetchType.LAZY);
            annotation.param(namingService.parameterName(ParameterKind.MAPPED_BY), "owner");
        }
        if (entityField.getFieldType().getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE) {
            annotation.param(namingService.parameterName(ParameterKind.FETCH), FetchType.EAGER);
            annotation.param(namingService.parameterName(ParameterKind.CASCADE), CascadeType.ALL);
            annotation.param(namingService.parameterName(ParameterKind.ORPHAN_REMOVAL), true);
        }

        JAnnotationUse columnAnnotation = null;
        if (fieldTypeKind == EntityTypeKind.PRIMITIVE && getEntityField().getFieldType().getPrimitiveEntityType() !=PrimitiveEntityType.URL_RESOURCE) {
            columnAnnotation = fieldVar.annotate(Column.class);
            columnAnnotation.param(namingService.parameterName(ParameterKind.NULLABLE), true);
        }
    }

    private JType getFieldClass(TypeModel fieldTypeModel) {

        switch (entityField.getFieldKind()) {
            case COLLECTION:
                return owner.getPackage().owner().ref(Set.class).narrow(fieldTypeModel.getType());
            case REGULAR:
                return fieldTypeModel.getType();
        }
        return null;
    }

    private Class<? extends Annotation> getFieldAnnotationClass(EntityType fieldType) {
        switch (entityField.getFieldKind()) {
            case REGULAR:
                return getRegularFieldAnnotationClass(fieldType);
            case COLLECTION:
                return getCollectionFieldAnnotationClass(fieldType);
        }
        throw new RuntimeException("missing field type ? ");
    }

    private Class<? extends Annotation> getRegularFieldAnnotationClass(EntityType fieldType) {

        switch (fieldType.getTypeKind()) {
            case PRIMITIVE:
                if (fieldType.getPrimitiveEntityType() == PrimitiveEntityType.URL_RESOURCE) {
                    return OneToOne.class;
                }
                return Basic.class;
            default:
                return ManyToOne.class;
        }
    }


    private Class<? extends Annotation> getCollectionFieldAnnotationClass(EntityType fieldType) {

        switch (fieldType.getTypeKind()) {
            case PRIMITIVE:
                throw new CodeModelException();
            default:
                return OneToMany.class;
        }
    }

    private void createGetter() {

        NamingService namingService = owner.getCodeModel().getNamingService();
        String getterName = namingService.entityFieldGetterName(entityField);

        getterMethod = owner.getDefinedClass().method(JMod.PUBLIC, fieldVar.type(), getterName);
        getterMethod.body()._return(fieldVar);
    }

    private void createSetter() {

        NamingService namingService = owner.getCodeModel().getNamingService();
        String setterName = namingService.entityFieldSetterName(entityField);
        setterMethod = owner.getDefinedClass().method(JMod.PUBLIC, void.class, setterName);
        JVar value = setterMethod.param(fieldVar.type(), fieldVar.name());
        setterMethod.body().assign(JExpr._this().ref(fieldVar), value);
    }

    public TypeModel getOwner() {
        return owner;
    }

    public EntityField getEntityField() {
        return entityField;
    }
}
