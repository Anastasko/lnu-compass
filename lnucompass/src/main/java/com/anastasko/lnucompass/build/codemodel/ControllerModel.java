package com.anastasko.lnucompass.build.codemodel;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityFieldKind;
import com.anastasko.lnucompass.build.model.enums.FieldKind;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.build.model.enums.ParameterKind;
import com.anastasko.lnucompass.build.model.enums.VariableKind;
import com.anastasko.lnucompass.validation.exceptions.ResourceNotFoundException;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

import io.swagger.annotations.Api;

public class ControllerModel extends AbstractEntityClassModel<ControllerModel> implements ModelBuilder {

    private JType itemViewModelType;
    private JType itemType;

    public ControllerModel(EntityType entityType, ControllerPackageModel controllerPackageModel)
            throws JClassAlreadyExistsException {
        super(entityType, controllerPackageModel);

        NamingService namingService = getCodeModel().getNamingService();
        String controllerName = getCodeModel().getNamingService().entityTypeControllerName(entityType);

        definedClass = getPackage()._class(controllerName);
        definedClass.annotate(Api.class)
                .param(namingService.fieldName(FieldKind.TAGS), namingService.entityTypeSimpleSpacedName(getEntityType()));
        definedClass.annotate(RestController.class);
        definedClass.annotate(RequestMapping.class).param(namingService.parameterName(ParameterKind.VALUE),
                getCodeModel().getNamingService().entityTypeControllerRequestMappingValue(entityType));

    }

    @Override
    public void build() {

        ViewModelModel viewModelModel = getCodeModel().getViewModelPackageModel().getClassModel(getEntityType());
        this.itemViewModelType = viewModelModel.getType();

        TypeModel typeModel = getCodeModel().getTypePackageModel().getClassModel(getEntityType());
        this.itemType = typeModel.getType();

        createFindAllMethod();
        createCreateMethod();
        createFindManyMethod();
        createFindOneMethod();
        createFindAllCollections();         // e.g. cityItem/{id}/maps
        createUpdateMethod();
        createDeleteMethod();
    }

    private void createFindAllMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, ref(List.class).narrow(itemViewModelType),
                namingService.methodName(MethodKind.FIND_ALL));
        method.annotate(GetMapping.class);
        method.body()._return(getAutowiredViewServiceField(getEntityType()).invoke(namingService.methodName(MethodKind.FIND_ALL)));
    }

    private void createFindManyMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, ref(List.class).narrow(itemViewModelType),
                namingService.methodName(MethodKind.FIND_MANY));
        method.annotate(PostMapping.class)
                .param(namingService.parameterName(ParameterKind.VALUE),
                        "/" + namingService.parameterName(ParameterKind.FIND_MANY));
        JVar idsArg = method.param(ref(List.class).narrow(Long.class), namingService.variableName(VariableKind.IDS));
        idsArg.annotate(RequestBody.class);

        method.body()._return(getAutowiredViewServiceField(getEntityType())
                .invoke(namingService.methodName(MethodKind.FIND_MANY))
                .arg(idsArg));
    }

    private void createFindOneMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, itemViewModelType,
                namingService.methodName(MethodKind.FIND_ONE));
        method.annotate(GetMapping.class)
                .param(namingService.parameterName(ParameterKind.VALUE), "/{" + namingService.parameterName(ParameterKind.ID).toLowerCase() + "}");

        JVar argId = method.param(Long.class, namingService.variableName(VariableKind.ID));
        argId.annotate(PathVariable.class).param(namingService.parameterName(ParameterKind.VALUE),
                namingService.variableName(VariableKind.ID));

        method.body()._return(getAutowiredViewServiceField(getEntityType())
                .invoke(namingService.methodName(MethodKind.FIND_ONE)).arg(argId));
    }

    private void createCreateMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, Long.class,
                namingService.methodName(MethodKind.CREATE));
        method.annotate(PostMapping.class);
        JVar viewModel = method.param(itemViewModelType, namingService.variableName(VariableKind.ITEM));
        viewModel.annotate(RequestBody.class);
        JFieldVar viewService = getAutowiredViewServiceField(getEntityType());
        method.body()._return(viewService.invoke(namingService.methodName(MethodKind.CREATE)).arg(viewModel));
    }

    private void createUpdateMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, void.class,
                namingService.methodName(MethodKind.UPDATE));
        method.annotate(PutMapping.class);
        JVar viewModel = method.param(itemViewModelType, namingService.variableName(VariableKind.ITEM));
        viewModel.annotate(RequestBody.class);
        JFieldVar viewService = getAutowiredViewServiceField(getEntityType());
        method.body().add(viewService.invoke(namingService.methodName(MethodKind.UPDATE)).arg(viewModel));
    }

    private void createDeleteMethod() {
        JMethod method = definedClass.method(JMod.PUBLIC, void.class,
                namingService.methodName(MethodKind.DELETE));
        method.annotate(DeleteMapping.class)
                .param(namingService.parameterName(ParameterKind.VALUE), "/{" + namingService.parameterName(ParameterKind.ID).toLowerCase() + "}");

        JVar argId = method.param(Long.class, namingService.variableName(VariableKind.ID));
        argId.annotate(PathVariable.class).param(namingService.parameterName(ParameterKind.VALUE),
                namingService.variableName(VariableKind.ID));

        JFieldVar viewService = getAutowiredViewServiceField(getEntityType());
        method.body().add(viewService.invoke(namingService.methodName(MethodKind.DELETE)).arg(argId));
    }

    private void createFindAllCollections() {
        getEntityType().getFields().stream().filter(f -> f.getFieldKind() == EntityFieldKind.COLLECTION).forEach(f -> {
            JType fieldViewModelType = getCodeModel().getViewModelPackageModel().getClassModel(f.getFieldType()).getType();
            JType collectionViewModelType = ref(List.class).narrow(fieldViewModelType);
            JMethod method = definedClass.method(JMod.PUBLIC, collectionViewModelType,
                    namingService.methodName(MethodKind.FIND) + "_" + namingService.entityFieldName(f));
            method.annotate(GetMapping.class)
                    .param(namingService.parameterName(ParameterKind.VALUE),
                            "/{" + namingService.parameterName(ParameterKind.ID).toLowerCase() + "}" +
                                    "/" + namingService.entityFieldName(f));

            JVar argId = method.param(Long.class, namingService.variableName(VariableKind.ID));
            argId.annotate(PathVariable.class).param(namingService.parameterName(ParameterKind.VALUE),
                    namingService.variableName(VariableKind.ID));

            JFieldVar entityService = getAutowiredServiceField(getEntityType());
            JVar item = method.body().decl(itemType, namingService.variableName(VariableKind.ITEM),
                    entityService.invoke(namingService.methodName(MethodKind.FIND_ONE)).arg(argId).arg(namingService.entityFieldGraphName(f)));

            method.body()._if(item.eq(JExpr._null()))._then().
                    _throw(JExpr._new(ref(ResourceNotFoundException.class))
                            .arg(JExpr.lit(namingService.resourceNotFoundExceptionMessage(getEntityType()))
                                    .plus(argId)));

            method.body()._return(getAutowiredViewServiceField(f.getFieldType())
                    .invoke(namingService.methodName(MethodKind.VIEW_MODELS))
                    .arg(item.invoke(namingService.entityFieldGetterName(f))));
        });
    }

}
