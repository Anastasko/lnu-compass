package com.anastasko.lnucompass.build.model.enums;

public enum EntityFieldKind {
	REGULAR,
	COLLECTION
}
