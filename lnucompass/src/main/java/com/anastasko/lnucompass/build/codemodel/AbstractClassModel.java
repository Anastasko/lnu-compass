package com.anastasko.lnucompass.build.codemodel;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;
import com.sun.codemodel.JType;

public class AbstractClassModel<T extends AbstractClassModel<T>> {

	private final JPackage pack;
	private final CodeModel codemodel;
	@SuppressWarnings("rawtypes")
	private AbstractEntityPackageModel owner;

	protected JDefinedClass definedClass;

	protected NamingService namingService;

	protected JFieldVar getAutowiredServiceField(EntityType entityType) {
		if(!autowiredServiceFieldMap.containsKey(entityType.getId())) {
			String fieldName = codemodel.getNamingService().entityTypeServiceFieldName(entityType);
			JFieldVar fieldVar = definedClass.field(JMod.PRIVATE,
					codemodel.getServicePackageModel().getClassModel(entityType).getType(),
					fieldName);
			fieldVar.annotate(Autowired.class);
			autowiredServiceFieldMap.put(entityType.getId(), fieldVar);
		}
		
		return autowiredServiceFieldMap.get(entityType.getId());
	}

	protected JFieldVar getAutowiredViewServiceField(EntityType entityType) {
		if(!autowiredViewServiceFieldMap.containsKey(entityType.getId())) {
			String fieldName = codemodel.getNamingService().entityTypeViewServiceFieldName(entityType);
			JFieldVar fieldVar = definedClass.field(JMod.PRIVATE,
					codemodel.getViewServicePackageModel().getClassModel(entityType).getType(),
					fieldName);
			fieldVar.annotate(Autowired.class);
			autowiredViewServiceFieldMap.put(entityType.getId(), fieldVar);
		}

		return autowiredViewServiceFieldMap.get(entityType.getId());
	}

	@SuppressWarnings("rawtypes")
	protected JClass ref(Class class1) {
		return getPackage().owner().ref(class1);
	}
	
	protected JType getType() {
		return definedClass;
	}
	
	protected JClass getJClass() {
		return definedClass;
	}

	protected JDefinedClass getDefinedClass() {
		return definedClass;
	}

	protected JPackage getPackage() {
		return pack;
	}

	protected CodeModel getCodeModel() {
		return codemodel;
	}

	public AbstractClassModel(JPackage pack, CodeModel codemodel) {
		this.pack = pack;
		this.namingService = codemodel.getNamingService();
		this.codemodel = codemodel;
	}
	public AbstractClassModel(AbstractEntityPackageModel<?> owner) {
		this(owner.getPackage(), owner.getCodeModel());
		this.owner = owner;
	}

	private Map<Long, JFieldVar> autowiredServiceFieldMap = new HashMap<>();
	private Map<Long, JFieldVar> autowiredViewServiceFieldMap = new HashMap<>();

	public AbstractEntityPackageModel<?> getOwner() {
		return owner;
	}
}
