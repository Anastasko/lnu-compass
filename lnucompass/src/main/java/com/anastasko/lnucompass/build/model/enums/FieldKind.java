package com.anastasko.lnucompass.build.model.enums;

public enum FieldKind {

    GENERATED_ENTITY_SERVICE, ENTITY_TYPE_SERVICE, OBJECT_MAPPER, NAME, DESCRIPTION, ID, URL_RESOURCE_VIEW_SERVICE, TAGS, NAMING_SERVICE

}

