package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JPackage;

public class ServicePackageModel extends AbstractEntityPackageModel<ServiceModel> implements ModelBuilder {
	
	public ServicePackageModel(Iterable<EntityType> entityTypes, JPackage pack, CodeModel codeModel) throws JClassAlreadyExistsException, ClassNotFoundException {

		super(codeModel, pack);

		for (EntityType entityType : entityTypes) {
			EntityTypeKind entityTypeKind = entityType.getTypeKind();
			if(entityTypeKind == EntityTypeKind.ENTITY)
				addClassModel(entityType, new ServiceModel(entityType, this));
		}
	}
	
	@Override
	public void build() {
		
		for (ServiceModel classModel : getClassModels()) {
			classModel.build();
		}
	}
}
