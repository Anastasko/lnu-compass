package com.anastasko.lnucompass.build.model.view;

import java.util.ArrayList;
import java.util.List;

import com.anastasko.lnucompass.build.implementation.NamingServiceImpl;
import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.EntityTypeKind;
import com.anastasko.lnucompass.build.model.enums.PrimitiveEntityType;
import com.anastasko.lnucompass.model.view.AbstractEntityViewModel;

public class EntityTypeViewModel extends AbstractEntityViewModel {

    private String name;

    private EntityTypeKind kind;

    private PrimitiveEntityType primitiveEntityType;

    private List<EntityFieldViewModel> fields = new ArrayList<>();

    public EntityTypeViewModel(EntityType type) {
        super(type);
        NamingService nm = new NamingServiceImpl();
        setName(nm.entityTypeSimpleName(type, false));
        setKind(type.getTypeKind());
        setPrimitiveEntityType(type.getPrimitiveEntityType());
        type.getFields().stream().forEach(f -> getFields().add(new EntityFieldViewModel(f)));
    }

    public String getName() {
        return name;
    }

    public void setName(String typeName) {
        this.name = typeName;
    }

    public EntityTypeKind getKind() {
        return kind;
    }

    public void setKind(EntityTypeKind typeKind) {
        this.kind = typeKind;
    }

    public PrimitiveEntityType getPrimitiveEntityType() {
        return primitiveEntityType;
    }

    public void setPrimitiveEntityType(PrimitiveEntityType primitiveEntityType) {
        this.primitiveEntityType = primitiveEntityType;
    }

    public List<EntityFieldViewModel> getFields() {
        return fields;
    }

    public void setFields(List<EntityFieldViewModel> fields) {
        this.fields = fields;
    }

}
