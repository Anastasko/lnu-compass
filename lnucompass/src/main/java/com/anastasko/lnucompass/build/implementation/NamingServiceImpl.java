package com.anastasko.lnucompass.build.implementation;

import com.anastasko.lnucompass.build.infrastructure.NamingService;
import com.anastasko.lnucompass.build.model.domain.EntityField;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.anastasko.lnucompass.build.model.enums.FieldKind;
import com.anastasko.lnucompass.build.model.enums.MethodKind;
import com.anastasko.lnucompass.build.model.enums.ParameterKind;
import com.anastasko.lnucompass.build.model.enums.VariableKind;

public class NamingServiceImpl implements NamingService {

    private String toLowerCase(String item, boolean startWithUpperCase) {
        if (item.length() == 0) {
            return item;
        }
        return startWithUpperCase ? item.substring(0, 1).toUpperCase() + item.substring(1).toLowerCase() : item.toLowerCase();
    }

    @Override
    public String toCamelCase(Enum<?> item, boolean startWithUpperCase) {

        String[] parts = item.name().split("_");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; ++i) {
            sb.append(toLowerCase(parts[i], i != 0 || startWithUpperCase));
        }
        return sb.toString();
    }

    private String toSpacedCamelCase(Enum<?> item) {
        String[] parts = item.name().split("_");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; ++i) {
            if (i!=0){
                sb.append(" ");
            }
            sb.append(toLowerCase(parts[i], true));
        }
        return sb.toString();
    }

    @Override
    public String methodName(MethodKind methodKind) {
        return toCamelCase(methodKind, false);
    }

    @Override
    public String fieldName(FieldKind fieldKind) {
        return toCamelCase(fieldKind, false);
    }

    @Override
    public String entityTypeViewModelName(EntityType entityType) {
        return String.format("Entity%sViewModel", entityTypeSimpleName(entityType));
    }

    @Override
    public String resourceNotFoundExceptionMessage(EntityType entityType) {
        return entityTypeSimpleName(entityType) + " does not exist. id=";
    }

    @Override
    public String entityTypeViewServiceName(EntityType entityType) {
        return String.format("%sViewService", entityTypeSimpleName(entityType));
    }

    @Override
    public String entityTypeViewServiceImplName(EntityType entityType) {
        return String.format("%sViewServiceImpl", entityTypeSimpleName(entityType));
    }

    @Override
    public String entityTypeViewServiceFieldName(EntityType entityType) {
    		String name = entityTypeSimpleName(entityType, false);
        return String.format("%sViewService", name);
    }

    @Override
    public String entityFieldGraphName(EntityField f) {
        return entityFieldName(f) + "Graph";
    }

    @Override
    public String variableName(VariableKind variableKind) {
        return toCamelCase(variableKind, false);
    }

    @Override
    public String parameterName(ParameterKind parameterKind) {
        return toCamelCase(parameterKind, false);
    }

    @Override
    public String entityTypeName(EntityType entityType) {
        return String.format("Entity%s", entityTypeSimpleName(entityType));
    }

    @Override
    public String entityTypeSimpleName(EntityType entityType, boolean upperCaseFirstLetter) {
    		String name = entityType.getIsSystem()
        		? entityType.getSystemClass().getSimpleName()
        		: toCamelCase(entityType.getTypeName(), true);
        	if (upperCaseFirstLetter) {
        		return name;
        	}
        	return name.substring(0,1).toLowerCase() + name.substring(1);
    }
    
    public String entityTypeSimpleName(EntityType entityType) {
    		return entityTypeSimpleName(entityType, true);
    }

    @Override
    public String entityFieldName(EntityField field) {
        return toCamelCase(field.getFieldName(), false);
    }

    @Override
    public String entityFieldVarName(EntityField field) {
        return toCamelCase(field.getFieldName(), false);
    }

    @Override
    public String entityFieldGetterName(EntityField field) {
        return String.format("get%s", toCamelCase(field.getFieldName(), true));
    }

    @Override
    public String entityFieldSetterName(EntityField field) {
        return String.format("set%s", toCamelCase(field.getFieldName(), true));
    }

    @Override
    public String entityTypeServiceName(EntityType entityType) {
        return String.format("%sService", entityTypeSimpleName(entityType));
    }

    @Override
    public String entityTypeServiceImplName(EntityType entityType) {
        return String.format("%sServiceImpl", entityTypeSimpleName(entityType));
    }

    @Override
    public String systemTypeServiceClassName(EntityType entityType) throws ClassNotFoundException {
        return String.format(
                "com.anastasko.lnucompass.infrastructure.%sService",
                entityType.getSystemClass().getSimpleName());
    }

    @Override
    public String systemTypeServiceImplClassName(EntityType entityType) {
        return String.format(
                "com.anastasko.lnucompass.component.%sServiceImpl",
                entityType.getSystemClass().getSimpleName());
    }

    @Override
    public String entityTypeServiceFieldName(EntityType entityType) {
        String name = entityTypeSimpleName(entityType, false);
    	return String.format("%sService", name);
    }

    @Override
    public String entityTypeVarName(EntityType entityType) {
        return String.format("entity%d", entityTypeSimpleName(entityType));
    }

    @Override
    public String entityTypeVarName() {
        return String.format("entity");
    }

    @Override
    public String entityTypeControllerName(EntityType entityType) {
    		return String.format("%sController", entityTypeSimpleName(entityType));
    }

    @Override
    public String entityTypeControllerRequestMappingValue(EntityType entityType) {
    		return "/" + entityTypeSimpleName(entityType, false);
    }

    @Override
    public String emptyString() {
        return "";
    }

    @Override
    public String joinTableName(EntityField entityField) {
        return String.format("%s_%s", toCamelCase(entityField.getOwner().getTypeName(), false), toCamelCase(entityField.getFieldName(), false));
    }

    @Override
    public String entityGetterName(String variableName) {
        StringBuilder sb = new StringBuilder(variableName);
        for (int i = 0; i < sb.length(); i++) {
            if (Character.isLowerCase(sb.charAt(i))) {
                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
                break;
            }
        }
        return String.format("get" + sb.toString());
    }

    @Override
    public String entitySetterName(String variableName) {
        StringBuilder sb = new StringBuilder(variableName);
        for (int i = 0; i < sb.length(); i++) {
            if (Character.isLowerCase(sb.charAt(i))) {
                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
                break;
            }
        }
        return String.format("set" + sb.toString());
    }

	@Override
	public String systemTypeViewServiceClassName(EntityType entityType) {
		return String.format(
                "com.anastasko.lnucompass.infrastructure.%sViewService",
                entityType.getSystemClass().getSimpleName());
	}

	@Override
	public String systemTypeViewModelClassName(EntityType entityType) {
		return String.format(
                "com.anastasko.lnucompass.model.view.%sViewModel",
                entityType.getSystemClass().getSimpleName());
	}

    @Override
    public String entityTypeSimpleSpacedName(EntityType entityType) {
        return toSpacedCamelCase(entityType.getTypeName());
    }

    @Override
    public String entityTypeFieldSimpleSpacedName(EntityField entityField) {
        return toSpacedCamelCase(entityField.getFieldName());
    }

}
