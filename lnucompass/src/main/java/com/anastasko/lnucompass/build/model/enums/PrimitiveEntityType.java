package com.anastasko.lnucompass.build.model.enums;

import com.anastasko.lnucompass.model.domain.UrlResource;

public enum PrimitiveEntityType {

	STRING,
    DOUBLE, 
    INTEGER,
    URL_RESOURCE;

    public Class<?> getPrimitiveEntityTypeClass(boolean isNullable) {
        switch (this) {
            case INTEGER:
                return isNullable ? Long.class : long.class;
            case STRING:
                return String.class;
            case DOUBLE:
                return Double.class;
            case URL_RESOURCE:
                return UrlResource.class;
        }
        throw new RuntimeException(this + " primitive type ?");
    }
	
}
