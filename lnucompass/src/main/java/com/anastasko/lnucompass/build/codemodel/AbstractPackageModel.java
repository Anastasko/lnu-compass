package com.anastasko.lnucompass.build.codemodel;

import java.util.Collection;

import com.sun.codemodel.JPackage;

public abstract class AbstractPackageModel<T extends AbstractClassModel<T>> {

	protected abstract Collection<T> getClassModels(); 
	
	protected AbstractPackageModel(CodeModel codeModel, JPackage pack) {
		
		this.codeModel = codeModel;
		this.pack = pack;
	}

	protected  CodeModel getCodeModel() {
		return codeModel;
	}

	protected  JPackage getPackage() {
		return pack;
	}
	
	private CodeModel codeModel;
	private JPackage pack;
}
