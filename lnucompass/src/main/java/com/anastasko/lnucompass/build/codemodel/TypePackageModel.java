package com.anastasko.lnucompass.build.codemodel;

import com.anastasko.lnucompass.build.infrastructure.ModelBuilder;
import com.anastasko.lnucompass.build.model.domain.EntityType;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JPackage;

public class TypePackageModel extends AbstractEntityPackageModel<TypeModel> implements ModelBuilder {

	public TypePackageModel(Iterable<EntityType> entityTypes, JPackage pack, CodeModel codeModel) throws JClassAlreadyExistsException, ClassNotFoundException {
		super(codeModel, pack);
		for(EntityType entityType : entityTypes) {
			addClassModel(entityType, new TypeModel(entityType, this));
		}
	}
	
	@Override
	public void build() {
		for (TypeModel modelType : getClassModels()) {
			modelType.build();
		}
	}

}
