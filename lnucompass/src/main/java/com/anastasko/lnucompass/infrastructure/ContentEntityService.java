package com.anastasko.lnucompass.infrastructure;

import java.util.List;

import com.anastasko.lnucompass.model.domain.AbstractContentEntity;
import com.anastasko.lnucompass.model.view.SyncModels;

public interface ContentEntityService<T extends AbstractContentEntity> extends EntityPersistenceService<T> {

    List<T> findSync(SyncModels models);

    List<T> findAll(boolean withDeleted);

}
