package com.anastasko.lnucompass.infrastructure;

import java.util.Collection;
import java.util.List;

import com.anastasko.lnucompass.model.domain.AbstractEntity;
import com.anastasko.lnucompass.model.view.EntityViewModel;

public interface ViewService<T extends AbstractEntity, V extends EntityViewModel<Long>> {

    List<V> findAll();

    List<V> findMany(Iterable<Long> ids);

    V findOne(Long id);

    Long create(V model);

    void update(V model);

    void delete(Long id);

    List<V> viewModels(Collection<T> list);

    void mergeFields(T e, V v);

}
