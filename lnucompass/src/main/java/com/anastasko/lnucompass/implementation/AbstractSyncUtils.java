package com.anastasko.lnucompass.implementation;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.anastasko.lnucompass.api.infrastructure.service.CityItemService;
import com.anastasko.lnucompass.api.model.domain.EntityCityItem;

public class AbstractSyncUtils {

    @Autowired
    private CityItemService cityItemService;

    public List<EntityCityItem> sortedCityItems() {
        return cityItemService.findAll(true).stream()
                .sorted(Comparator.comparing(item -> item.getId()))
                .collect(Collectors.toList());
    }

}
