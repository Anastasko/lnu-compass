package com.anastasko.lnucompass.model.view;

import com.anastasko.lnucompass.model.domain.AbstractEntity;

public class AbstractEntitiesViewModel extends EntitiesViewModel<AbstractEntityViewModel> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AbstractEntitiesViewModel() {}
	
	public AbstractEntitiesViewModel(Iterable<? extends AbstractEntity> list){
		for(AbstractEntity entity : list){
			add(new AbstractEntityViewModel(entity));
		}
	}

}
