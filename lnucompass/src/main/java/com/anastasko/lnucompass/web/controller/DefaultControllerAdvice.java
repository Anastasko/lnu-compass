package com.anastasko.lnucompass.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.anastasko.lnucompass.validation.exceptions.WithHttpStatus;

@ControllerAdvice
public class DefaultControllerAdvice {

    @ExceptionHandler(WithHttpStatus.class)
    public ResponseEntity<String> handleResourceNotFoundException(WithHttpStatus e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }


}
