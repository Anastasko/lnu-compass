package com.anastasko.lnucompass.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.anastasko.lnucompass.infrastructure.PropertyService;
import com.anastasko.lnucompass.model.domain.AbstractEntity;
import com.anastasko.lnucompass.model.view.AbstractEntityViewModel;

public abstract class AbstractController<T extends AbstractEntity, V extends AbstractEntityViewModel> {

	@Autowired
	protected PropertyService properties;
	
	@Autowired
    protected MessageSource messageSource;

}
