package com.anastasko.lnucompass.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@GetMapping("/")
	public long home() {
	    return System.currentTimeMillis();
	}
	
}
