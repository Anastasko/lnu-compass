package com.anastasko.lnucompass.sync.compression;

public class CompressionException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompressionException(Throwable e){
        super(e);
    }

}
