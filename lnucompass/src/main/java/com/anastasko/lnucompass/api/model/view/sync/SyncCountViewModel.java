
package com.anastasko.lnucompass.api.model.view.sync;


public class SyncCountViewModel {

    private Long map;
    private Long faculty;
    private Long iosIcon;
    private Long mapItem;
    private Long itemKind;
    private Long cityItem;
    private Long androidIcon;

    public Long getMap() {
        return map;
    }

    public void setMap(Long cnt) {
        map = cnt;
    }

    public Long getFaculty() {
        return faculty;
    }

    public void setFaculty(Long cnt) {
        faculty = cnt;
    }

    public Long getIosIcon() {
        return iosIcon;
    }

    public void setIosIcon(Long cnt) {
        iosIcon = cnt;
    }

    public Long getMapItem() {
        return mapItem;
    }

    public void setMapItem(Long cnt) {
        mapItem = cnt;
    }

    public Long getItemKind() {
        return itemKind;
    }

    public void setItemKind(Long cnt) {
        itemKind = cnt;
    }

    public Long getCityItem() {
        return cityItem;
    }

    public void setCityItem(Long cnt) {
        cityItem = cnt;
    }

    public Long getAndroidIcon() {
        return androidIcon;
    }

    public void setAndroidIcon(Long cnt) {
        androidIcon = cnt;
    }

}
