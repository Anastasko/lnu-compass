#!/bin/bash
bash "/usr/local/bin/docker-entrypoint.sh" "postgres" &

TIMEOUT=15
until psql -U postgres -d postgres -c "select 1" > /dev/null 2>&1 || [ $TIMEOUT -eq 0 ]; do
  echo "Waiting for postgres server, $((TIMEOUT--)) remaining attempts..."
  sleep 1
done

echo "INITIALIZED.";
export TERM=xterm-256color
watch -n 3600 'bash /backup.sh'
