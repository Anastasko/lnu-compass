#!/bin/bash

USERNAME=postgres
DBHOST=localhost
DBNAMES="compass"
BACKUPDIR="/backups"
TMPDIR="/tmp"
CREATE_DATABASE=no
DOWEEKLY=7
DOMONTHLY=01
DODAILY=00


PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/postgres/bin:/usr/local/pgsql/bin
DATE=`date +%Y-%m-%d`				# Datestamp e.g 2002-09-21
HOD=`date +%H`					# Hour od day, e.g. 12
DOW=`date +%A`					# Day of the week e.g. Monday
DNOW=`date +%u`					# Day number of the week 1 to 7 where 1 represents Monday
DOM=`date +%d`					# Date of the Month e.g. 27
M=`date +%B`					# Month e.g January
W=`date +%V`					# Week Number e.g 37
LOGFILE=$TMPDIR/log.log				# Logfile Name
OPT=""						# OPT string for use with mysqldump


if [ ! -e "$TMPDIR" ]
	then
	mkdir -p "$TMPDIR"
fi


# IO redirection for logging.
touch $LOGFILE
exec 6>&1           # Link file descriptor #6 with stdout.
                    # Saves stdout.
exec > $LOGFILE     # stdout replaced with file $LOGFILE.




# Database dump function
dbdump () {
	pg_dump --username=$USERNAME $HOST $OPT $1 > $2
	return 0
}

# Compression function
compression () {
	gzip -f "$1"
	return 0
}

if [ "$CREATE_DATABASE" = "no" ]; then
	OPT="$OPT"
else
	OPT="$OPT --create"
fi

if [ "$DBHOST" = "localhost" ]; then
	DBHOST="`hostname -f`"
	HOST=""
else
	HOST="-h $DBHOST"
fi


echo Backup Start Time `date`
echo ======================================================================

for DB in $DBNAMES
do

	DB="`echo $DB | sed 's/%/ /g'`"

	if [ ! -e "$BACKUPDIR/$DB/hourly" ]
		then
		mkdir -p "$BACKUPDIR/$DB/hourly"
	fi
	if [ ! -e "$BACKUPDIR/$DB/daily" ]
		then
		mkdir -p "$BACKUPDIR/$DB/daily"
	fi
	if [ ! -e "$BACKUPDIR/$DB/weekly" ]
		then
		mkdir -p "$BACKUPDIR/$DB/weekly"
	fi
	if [ ! -e "$BACKUPDIR/$DB/monthly" ]
                then
                mkdir -p "$BACKUPDIR/$DB/monthly"
        fi

	# Monthly backup
	if [ $DOM = $DOMONTHLY ]; then

		echo Monthly Backup of Database \( $DB \)

		dbdump "$DB" "$TMPDIR/$M.$DATE.sql"
                compression "$TMPDIR/$M.$DATE.sql"
		eval mv  -v "$TMPDIR/$M.$DATE.sql.gz" "$BACKUPDIR/$DB/monthly/$M.$DATE.sql.gz"
	fi
	# Weekly backup
	if [ $DNOW = $DOWEEKLY ]; then
		echo Weekly Backup of Database \( $DB \)
		echo Rotating 5 weeks Backups...

		if [ "$W" -le 05 ];then
			REMW=`expr 48 + $W`
		elif [ "$W" -lt 15 ];then
			REMW=0`expr $W - 5`
		else
			REMW=`expr $W - 5`
		fi

		eval rm -fv "$BACKUPDIR/$DB//weekly/$REMW.*"
		dbdump "$DB" "$TMPDIR/$W.$DATE.sql"
		compression "$TMPDIR/$W.$DATE.sql"
		eval mv -v "$TMPDIR/$W.$DATE.sql.gz" "$BACKUPDIR/$DB/weekly/$W.$DATE.sql.gz"

	# Daily Backup
	fi	
	if [ $HOD = $DODAILY ]; then
		echo Daily Backup of Database \( $DB \)
		echo Rotating last weeks Backup...

		eval rm -fv "$BACKUPDIR/$DB/daily/$DOW.*"
		dbdump "$DB" "$TMPDIR/$DOW.$DATE.sql"
		compression "$TMPDIR/$DOW.$DATE.sql"
		eval mv -v "$TMPDIR/$DOW.$DATE.sql.gz" "$BACKUPDIR/$DB/daily/$DOW.$DATE.sql.gz"
	fi
	
	echo Hourly Backup of Database \( $DB \)
	echo Rotating last hours Backup...
	eval rm -fv "$BACKUPDIR/$DB/hourly/$HOD.*"
	dbdump "$DB" "$TMPDIR/$HOD.$DATE.sql"
	compression "$TMPDIR/$HOD.$DATE.sql"
	eval mv "$TMPDIR/$HOD.$DATE.sql.gz" "$BACKUPDIR/$DB/hourly/$HOD.$DATE.sql.gz"
	
done


echo Backup End `date`
echo ======================================================================


echo Total disk space used for backup storage..
echo Size - Location
echo `du -hs "$BACKUPDIR"`
echo


#Clean up IO redirection
exec 1>&6 6>&-      # Restore stdout and close file descriptor #6.

# Clean up Logfile
eval rm -f "$LOGFILE"

exit 0
